FROM openjdk:8-jdk-alpine3.9
ARG IMAGETAG
ENV TAG=$IMAGETAG
MAINTAINER "sarumanoj03@gmail.com"
RUN addgroup -S smith && adduser -S smith -G smith && chown -R smith:smith /home
COPY target/assignment-$IMAGETAG.jar entrypoint.sh /home/
USER smith
WORKDIR /home
EXPOSE 8090
ENTRYPOINT ["./entrypoint.sh"]
